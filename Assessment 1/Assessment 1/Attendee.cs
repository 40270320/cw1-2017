﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//********************************
//*				                 *
//*	    Steven Gibson		     *
//*	    40270320		         *
//*	    Assessment 1		     *
//*	    Last modified	04/10/16 *
//*	    Attendee Class		     *
//*				                 *
//********************************

// This class is a sub-class of Person, it has data only relvent to people attending the confernce


namespace Assessment_1
{
    


    public class Attendee:Person
    {
        private int attendee_ref;
        private string paper_title;
        private string reg_type;
        private bool paid;
        private bool presenting;
        private string institution_name;
        private string confernce_name;
        public int Attendee_ref
        {
            get { return attendee_ref; }

            set
            { attendee_ref = value; }
        }

        public string Paper_title
        {
            get { return paper_title; }
            set { paper_title = value; }
        }

        public string Reg_type
        {
            get { return reg_type; }
            set { reg_type = value; }
        }
    
        public bool Paid
        {
            get { return paid; }
            set { paid = value; }
        }

        public bool Presenting
        {
            get { return presenting; }
            set { presenting = value; }
        }

        public string Institution_name
        {
            get { return institution_name; }
            set { institution_name = value; }
        }

        public string Confernce_name
        {
            get { return confernce_name; }
            set { confernce_name = value; }
        }
    }
}
