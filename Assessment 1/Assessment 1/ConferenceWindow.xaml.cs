﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

//********************************
//*				                 *
//*	    Steven Gibson		     *
//*	    40270320		         *
//*	    Assessment 1		     *
//*	    Last modified	04/10/16 *
//*	    conference Window	     *
//*				                 *
//********************************


// This GUI lets the user eneter infomation to a new attendee object
// This also lets the user retive data from an attendee object
// The GUI also lets the user create an invoice and a certifate

namespace Assessment_1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class ConferenceWindow : Window
    {
        public ConferenceWindow()
        {
            InitializeComponent();
            
        }
        private int attendee_number = 0;
        private string id_number, fname, sname, paper, instituion_n, confernce_name, reg_t, pres, pay;
        private bool paid, presenting,valid=false;
        Attendee new_attendee = new Attendee();
        
        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            this.txtFname.Clear();
            this.txtSname.Clear();
            this.txtAttendee.Clear();
            this.txtInstituion.Clear();
            this.txtConference.Clear();
            this.cmbPaid.SelectedIndex = -1;
            this.cmbPresenter.SelectedIndex = -1;
            this.cmbReg.SelectedIndex = -1;
            this.txtPaper.Clear();
        }

        private void btnSet_Click(object sender, RoutedEventArgs e)
        {


            
            set_id(); // calls a method to set varibles that will make new object the object 
            validty_check(); // calls a method to valdate the data before creatign the object
            if (valid == true) // if the valdation checks are ok it will make the object here  and display a message box wih the object details
            {
                make_attendee(fname, sname, attendee_number);
                make_details(paid, presenting, paper, instituion_n, confernce_name, reg_t);
                Display(new_attendee);
            }
            
        }
        //sets varibles from the text and combo boxes
        private void set_id()
        {

            fname = txtFname.Text;
            sname = txtSname.Text;
            paid_check();
            presenting_check();
            paper = txtPaper.Text;
            instituion_n = txtInstituion.Text;
            confernce_name = txtConference.Text;
            reg_t = cmbReg.Text;

       
        }
        //statrs making the object
        private Attendee make_attendee(string f, string s ,int i)
        {

            
            new_attendee.Attendee_ref = i;
            new_attendee.Firstname = f;
            new_attendee.Surname = s;


            return new_attendee;
        }
        // makes the rest of the objects
        private Attendee make_details(bool p,bool p_r,string p_n, string ins_n,string c, string r)
        {
            new_attendee.Paid = p;
            new_attendee.Presenting = p_r;
            new_attendee.Institution_name = ins_n;
            new_attendee.Confernce_name = c;
            new_attendee.Reg_type = r;
            return new_attendee;
        }
        // Displays a message box with dteails of the object ** this is only for testing purposes**
        private void Display(Attendee a)
        {
            MessageBox.Show("Attendee Reference number: " + new_attendee.Attendee_ref +
                            "\nAttendee name: "+ new_attendee.Firstname+" " + new_attendee.Surname + 
                            "\nInstitution Name: "+ new_attendee.Institution_name + 
                            "\nConfernece Name: "+ new_attendee.Confernce_name + 
                            "\nRegistration Type:" + new_attendee.Reg_type +
                            "\nPaid: " + new_attendee.Paid +
                            "\nPresenting: " + new_attendee.Presenting +
                            "\nPaper Title: "+new_attendee.Paper_title);
        }

        // this checks to see if the attendee has paid
        private bool paid_check()
        {
           pay = cmbPaid.Text;
            if (pay == "Yes")
            {
                return paid = true;
            }
            else
            {
                return paid = false;
            }
        }

        // this checks if the attendee is presenting at the conference
        private bool presenting_check()
        {
            pres = cmbPresenter.Text;
            if (pres == "Yes")
            {
                return presenting = true;
            }
            else 
            {
                return presenting = false;
            }
        }

       
       // all the valdaion checks are done in this method
        private bool validty_check()
        {
            
            //name check
            if ((fname == "")||(sname == ""))
            {
                MessageBox.Show("Please enter full name");
                return valid = false;
            }

            //number check
            // if the data entered into the textbox is a a letter or symbol or is blank then the try will create a message box with an error 
            try
            {

                id_number = txtAttendee.Text;
                attendee_number = int.Parse(id_number);
                if ((attendee_number > 40000) && (attendee_number <= 60000))
                {


                }

                else
                {
                    MessageBox.Show("Please enter a different ID number \nBetween 40000 and 60000", "Id Number error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return valid = false;
                }

            }
            catch (Exception excep)
            {
                MessageBox.Show("Not a vaild number", "Id Number error", MessageBoxButton.OK, MessageBoxImage.Error);
                return valid = false;
            }

            //blank in instution name or confenerce name
            if ((instituion_n == "") || (confernce_name == ""))
            {
                MessageBox.Show("Please fill in all applicable fields");
                return valid = false;
            }
            
            //refrence check
            if (reg_t == "")
            {
                MessageBox.Show("What type of attendee is this?");
                return valid = false;
            }
                //paid check
            if (pay == "")
            {
                MessageBox.Show("Has this attendee paid?");
                return valid = false;
            }
            
            //presenting check
            if (pres=="")
            {
                MessageBox.Show("Is this attendee presenting?");
                return valid = false;

            }
            // paper check to see if the paper title should be  left blank or not
            
            if ((presenting == true) && (paper == ""))
            {
                MessageBox.Show("A paper title must be submitted");
                return valid = false;
            }
            

            if ((presenting == false) && (paper != ""))
            {
                MessageBox.Show("A paper title must be blank");
                return valid = false;
            }
            else
                return valid = true;


        }
    }
}
