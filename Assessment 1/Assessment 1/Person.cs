﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//********************************
//*				                 *
//*	    Steven Gibson		     *
//*	    40270320		         *
//*	    Assessment 1		     *
//*	    Last modified	04/10/16 *
//*	    Person Class		     *
//*				                 *
//********************************

// This is the base class has a first name and last name, this could be extended to hold more data
// at a later date such as phone number or address

namespace Assessment_1
{
    public class Person
    {
        private string firstname = "";
        private string surname = "";

        public string Firstname
        {
            get 
            { return firstname; }
        
            set
            { firstname = value; }

        }

        public string Surname
        {
            get { return surname; }
            set { surname = value; }
        }
    }

    
}
